<?php

if (!class_exists('ACT_Redirect')) {

    class ACT_Redirect {

        var $options;
        var $url;

        function __construct($options) {
            $this->options = $options;
            add_action('init', array($this, 'initialize'),10);
            add_action('init', array($this, 'add_rewrite_tags'),15);


            // Add rewrite rules
            add_action('generate_rewrite_rules', array($this, 'generate_rewrite_rules'));

            add_action('template_redirect', array($this, 'do_redirect'));
        }
        
        function initialize(){
            $url = $this->options->g($this->options->prefix . '_get_url');
            if (empty($url)) {
                $this->url = 'recommends';
            } else {
                $this->url = $url;
            }
        }
        
        function add_rewrite_tags() {
            add_rewrite_tag('%' . $this->url . '%', '([^/]*)');
        }

        function generate_rewrite_rules($wp_rewrite) {
            $rules = array(
                // Create rewrite rules for each action
                $this->url . '/([^/]+)/?$' =>
                'index.php?' . $this->url . '=' . $wp_rewrite->preg_index(1),
            );

            // Merge new rewrite rules with existing
            $wp_rewrite->rules = array_merge($rules, $wp_rewrite->rules);

            return $wp_rewrite;
        }

        function do_redirect() {
            $goto = get_query_var($this->url);
            if ($goto) {
                $amazon_redirect = new ToAmazon($this->options);
                $amazon_redirect->load_by_value('post_name', $goto);
                if ($amazon_redirect->key_value()) {
                    $red_url = $this->get_url($amazon_redirect->target_url);
                } else {
                    $red_url = $this->get_url();
                }
                wp_redirect($red_url);
                exit();
            }
        }

        private function get_url($long_tail = NULL) {
            $tag_id = $this->options->g($this->options->prefix . '_tag_id');
            if (empty($long_tail)) {
                $long_tail = "?tag=" . $tag_id;
            } else {
                if (substr($long_tail, 0, 1) !== '/') {
                    $long_tail = '/' . $long_tail;
                }
                if (strpos($long_tail, '?') === FALSE) {
                    $long_tail .= "?tag=" . $tag_id;
                } else if (strpos($long_tail, 'tag=') === FALSE) {
                    $long_tail .= "&tag=" . $tag_id;
                }
            }
            if (!session_id()) {
                session_start();
            }
            if (isset($_SESSION['act_location'])) {
                $country = $_SESSION['act_location'];
            } else {
                $ip = $_SERVER['REMOTE_ADDR'];
                $details = array('Fail!');
                if ($ip) {
                    $details = json_decode(file_get_contents("http://ipinfo.io/" . $ip . "/json"), TRUE);
                    if ($details && isset($details['country'])) {
                        $country = $details['country'];
                        $_SESSION['act_location'] = $country;
                    } else {
                        $country = 'US';
                        $_SESSION['act_location'] = 'US';
                    }
                } else {
                    $country = 'US';
                }
            }
            $country_urls = array(
                'BR' => 'com.br',
                'CN' => 'cn',
                'CA' => 'ca',
                'FR' => 'fr',
                'DE' => 'de',
                'IT' => 'it',
                'JP' => 'jp',
                'MX' => 'com.mx',
                'ES' => 'es',
                'GB' => 'co.uk',
                'IN' => 'in',
            );
            $lead_in = 'http://www.amazon.';
            if (key_exists($country, $country_urls)) {
                return $lead_in . $country_urls[$country] . $long_tail;
            } else {
                return $lead_in . 'com' . $long_tail;
            }
        }

    }

}