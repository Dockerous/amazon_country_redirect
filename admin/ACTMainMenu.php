<?php
if (!defined('ABSPATH'))
    exit;
if (!class_exists('ACT_Main_Menu')) {


    class ACT_Main_Menu extends Base_Menu {

        var $options;
        var $menu_title;
        var $page_title;
        var $page_headline;

        function __construct($options) {
            /* Add Custom Admin Menu */
            $this->options = $options;
            $this->page_id = $options->prefix . '_main';
            $this->menu_title = "Amazon Redirect Settings";
            $this->page_title = "Amazon Redirect Settings";
            $this->page_headline = "Amazon Redirect Settings";
            add_action('admin_init', array($this, 'register_settings'));
            add_action('admin_menu', array($this, 'admin_menu'));
            add_action('update_option_'.$this->options->prefix . '_main', array($this,'flush_rewrite'), 10, 2);
            add_action('add_option_'.$this->options->prefix . '_main', array($this,'flush_rewrite'), 10, 2);
            
            parent::__construct($options, TRUE);
        }

        function admin_menu() {
            global $toamazon_post_type;
            add_submenu_page(
                    'edit.php?post_type='.$toamazon_post_type->get_post_type_name(), // Register this submenu with the menu defined above
                    $this->page_title, // The text to the display in the browser when this menu item is active
                    'Settings', // The text for this menu item
                    'manage_options', // Which type of users can see this menu
                    $this->page_id, // The unique ID - the slug - for this menu item
                    array($this, 'settings_page')   // The function used to render the menu for this page to the screen
            );
        }

        function register_settings() {
            // Add the settings sections so we can add our
            // fields to it
            add_settings_section(
                    $this->options->prefix . '_settings_section', 'Settings', array($this, 'settings_section_output'), $this->page_id
            );

            // Add the fields with the names and function to use for the
            // settings, put it in their section
            add_settings_field(
                    $this->options->prefix . '_get_url', 'Custom Redirect URL', array($this, 'get_url_output'), $this->page_id, $this->options->prefix . '_settings_section'
            );

            add_settings_field(
                    $this->options->prefix . '_tag_id', 'Amazon Affiliate ID', array($this, 'tag_id_output'), $this->page_id, $this->options->prefix . '_settings_section'
            );

            // Register our setting so that $_POST handling is done for us and
            // our callback function just has to echo the <input>
            register_setting($this->page_id, $this->page_id, array($this, 'validate'));
        }

        function validate($values) {
            if (isset($values[$this->options->prefix . '_reset_token']) &&
                    $values[$this->options->prefix . '_reset_token'] == 1) {
                unset($values[$this->options->prefix . '_reset_token']);
                $values[$this->options->prefix . '_accessToken'] = '';
                $values[$this->options->prefix . '_accessTokenSecret'] = '';
            }
            return $values;
        }

        function get_url_output($args) {
            $key = $this->options->prefix . '_get_url';
            $subtext = '<p>Enter a custom location for your redirect URL.</p>';
            $this->output_input($key, $subtext, 60);
        }

        function tag_id_output($args) {
            $key = $this->options->prefix . '_tag_id';
            $subtext = '<p>Enter your amazon affiliate ID here.</p>';
            $this->output_input($key, $subtext, 60);
        }

        function settings_section_output() {
            
        }
        
        function flush_rewrite($old_value, $value){
            global $act_options;
            $act_options->init();
            global $act_redirect;
            $act_redirect->initialize();
            flush_rewrite_rules();
        }

        function settings_page() {
            if (!current_user_can('manage_options')) {
                wp_die('You do not have sufficient permissions to access this page.');
            }
            $this->open($this->page_headline);
            ?>
            <form method="post" action="options.php">
            <?php settings_fields($this->page_id); ?>
            <?php do_settings_sections($this->page_id); ?>          
            <?php submit_button(); ?>
            </form>
            <?php
            $this->close();
        }

    }

}