<?php

/*
 * Plugin Name: Amazon Country Redirect
 * Version: 0.1.0
 * Description: Allows you to create redirect URLs for Amazon links that determine the visitor's country sends them to the correct Amazon site. 
 * Plugin URI: https://gitlab.com/Dockerous/amazon_country_redirect
 * Author: Dockerous
 * Author URI: https://gitlab.com/Dockerous
 *
*/

require_once 'admin/options.php';
global $act_options;
$act_options = new Base_Options("act");
require_once 'classes/ClassBase.php';
require_once 'classes/ClassToAmazon.php';
require_once 'admin/menu_class.php';
require_once 'admin/ACTMainMenu.php';
global $act_main_menu;
$act_main_menu = new ACT_Main_Menu($act_options);
require_once 'redirect.php';
global $act_redirect;
$act_redirect = new ACT_Redirect($act_options);
require_once 'post_types/post_type_base.php';
require_once 'post_types/post_type_toamazon.php';
global $toamazon_post_type;
$toamazon_post_type = new Post_Type_ToAmazon($act_options);
