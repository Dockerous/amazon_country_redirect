<?php

if (!class_exists('Post_Type_ToAmazon')) {

    class Post_Type_ToAmazon extends Post_Type_Base {

        function __construct($options) {
            $this->set_label('toamazon');
            parent::__construct($options);
            $this->shortcode = 'toamazon';
            add_shortcode($this->shortcode, array($this, 'execute_shortcode'));
        }

        function init() {
            $this->set_post_type_name();
            $this->set_post_capability_type();
            $this->set_post_type_label('Amazon Redirect');
            $labels = array(
                'name' => _x($this->get_post_type_label() . 's', 'post type general name'),
                'singular_name' => _x($this->get_post_type_label(), 'post type singular name'),
                'add_new' => _x('Add New', $this->get_post_type_label()),
                'add_new_item' => __('Add New ' . $this->get_post_type_label()),
                'edit_item' => __('Edit ' . $this->get_post_type_label()),
                'new_item' => __('New ' . $this->get_post_type_label()),
                'view_item' => __('View ' . $this->get_post_type_label()),
                'search_items' => __('Search ' . $this->get_post_type_label()),
                'not_found' => __('Nothing found'),
                'not_found_in_trash' => __('Nothing found in Trash'),
                'parent_item_colon' => ''
            );

            $args = array(
                'label' => $this->get_post_type_label(),
                'labels' => $labels,
                'supports' => array('title'),
                'hierarchical' => false,
                'public' => false,
                'show_ui' => true,
                'show_in_menu' => true,
                'menu_position' => 5,
                'show_in_admin_bar' => false,
                'show_in_nav_menus' => false,
                'can_export' => true,
                'has_archive' => false,
                'exclude_from_search' => true,
                'publicly_queryable' => false,
                'rewrite' => false,
                'capability_type' => 'page',
            );


            register_post_type($this->get_post_type_name(), $args);
        }

        function execute_shortcode($atts = array()) {
            $url = $this->options->g($this->options->prefix . '_get_url');
            $site = site_url('/' . $url . '/');
            $loadable = array('slug' => 'post_name', 'id' => 'post_id');
            $a = shortcode_atts(array(
                'id' => FALSE,
                'slug' => FALSE,
                    ), $atts);
            $item = new Manage_Redirect($this->options);
            foreach ($loadable as $k => $v) {
                $value = $a[$k];
                if ($value) {
                    $item->load_by_value($v, $value);
                    continue;
                }
            }
            if (empty($item->key_value) || empty($item->url)) {
                $site .= 'home/';
            } else {
                $site .= $item->url . '/';
            }
            return $site;
        }

        function save_post($post_id, $post, $update) {
            $valid = parent::save_post($post_id, $post);
            if ($valid !== FALSE) {
                $url = filter_input(INPUT_POST, $this->options->prefix . '_target_url');
                $this->update_meta($post_id, $this->options->prefix . '_target_url', $url);
                $slug = filter_input(INPUT_POST, $this->options->prefix . '_slug');
                $slug = $this->clean_slug($slug);
                if ($slug && $post->post_name == $slug) {
                    $this->update_meta($post_id, $this->options->prefix . '_slug', $slug);
                }
            } else {
                return $valid;
            }
            return TRUE;
        }

        function meta_boxes_setup() {
            parent::meta_boxes_setup();
            add_filter('wp_insert_post_data', array($this, 'change_slug'), 10, 2);
        }

        function change_slug($data, $postarr) {
            $slug = $this->clean_slug($postarr[$this->options->prefix . '_slug']);
            if ($slug) {
                $data['post_name'] = $slug;
            }
            return $data;
        }

        function clean_slug($slug) {
            $slug = preg_replace('/[^A-Za-z0-9-.]/', '', $slug);
            if (!empty($slug)) {
                $args = array(
                    'post_type' => $this->post_type_name,
                    'numberposts' => 2,
                    'name' => $slug,
                );
                $my_posts = get_posts($args);
                if (count($my_posts) == 0 || (count($my_posts) < 2) && $my_posts[0]->ID == $post_id) {
                    return TRUE;
                }
            }
            return FALSE;
        }

        function meta_boxes_add() {
            add_meta_box(
                    $this->options->prefix . '_link_settings', // Unique ID
                    esc_html__('Link Settings'), // Title
                    array($this, 'meta_box_link_settings'), // Callback function
                    $this->get_post_type_name(), // Admin page (or post type)
                    'normal', // Context
                    'core'      // Priority
            );
            add_meta_box(
                    $this->options->prefix . '_link_info', // Unique ID
                    esc_html__('Link Info'), // Title
                    array($this, 'meta_box_link_info'), // Callback function
                    $this->get_post_type_name(), // Admin page (or post type)
                    'normal', // Context
                    'core'      // Priority
            );
        }

        function meta_box_link_settings($object, $box) {
            wp_nonce_field($this->get_post_type_name() . '_nonce_action', $this->get_post_type_name() . '_nonce');
            echo '<h4>Product URL</h4>';
            $key = $this->options->prefix . '_target_url';
            $subtext = '<p>Enter a custom location for your redirect URL.</p>';
            $this->output_input($object, $key, $subtext, 60);

            echo '<h4>Link Cloaker Slug</h4>';
            $key = $this->options->prefix . '_slug';
            $subtext = '<p>This is the slug for you referral url. (Use only A-Z,a-z,0-9,-_.)</p>';
            $this->output_input($object, $key, $subtext, 60);
        }

        function meta_box_link_info($object, $box) {
            if ($object->post_name) {
                echo "ID Shortcode: [" . $this->shortcode . ' id=' . $object->ID . "]<br>";
                echo "Slug Shortcode: [" . $this->shortcode . ' slug=' . $object->post_name . "]<br>";
                $url = $this->options->g($this->options->prefix . '_get_url');
                if (empty($url)) {
                    global $act_redirect;
                    $url = $act_redirect->url;
                }
                $site = site_url('/' . $url . '/');
                $full_url = $site . $object->post_name . '/';
                echo "URL: <a target='_blank' href='" . $full_url . "'>" . $full_url . "</a><br>";
            } else {
                echo 'Please save new Amazon Redirect to see the shortcodes and url.';
            }
        }

        function output_input($object, $key, $subtext = '', $size = NULL, $disabled = FALSE) {
            $value = get_post_meta($object->ID, $key, true);
            $size_str = '';
            if (isset($size) && is_int($size)) {
                $size_str = ' size="' . $size . '"';
            }
            $d = '';
            if ($disabled) {
                $d = ' disabled="disabled" ';
            }
            echo '<input type="text" name="' . $key . '" id="' . $key . '" value="' . $value . '" ' . $size_str . $d . '/>';
            echo $subtext;
        }

    }

}